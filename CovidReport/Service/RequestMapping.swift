//
//  RequestMapping.swift
//  CovidReport
//
//  Created by Roberto Padilha on 25/09/20.
//  Copyright © 2020 Roberto Padilha. All rights reserved.
//

import Foundation
import Alamofire

public let SERVICE_GET_TOTALS = "https://covid-19-statistics.p.rapidapi.com/reports/total"
public let SERVICE_GET_COUNTRY = "https://covid-19-tracking.p.rapidapi.com/v1/"

public let COVID_STATISTICS_HEADERS : HTTPHeaders = [
    "x-rapidapi-host": "covid-19-statistics.p.rapidapi.com",
    "x-rapidapi-key": "e0f1538e16mshf2f23b83c143ad0p1cdcb8jsn5e228ab5955f",
    "useQueryString": "true"
]

public let COVID_TRACKING_HEADERS : HTTPHeaders = [
    "x-rapidapi-host": "covid-19-tracking.p.rapidapi.com",
    "x-rapidapi-key": "04357ecc16msh3f6b6e37c56b566p1986d4jsn7dcab86884cc",
    "useQueryString": "true"
]
