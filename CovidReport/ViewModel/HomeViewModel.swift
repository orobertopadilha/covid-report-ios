//
//  HomeViewModel.swift
//  CovidReport
//
//  Created by Roberto Padilha on 25/09/20.
//  Copyright © 2020 Roberto Padilha. All rights reserved.
//

import Foundation

final class HomeViewModel : ObservableObject{
    
    @Published public var total: String = "0"
    @Published public var deaths: String = "0"
    @Published public var recovered: String = "0"
    @Published public var recovering: String = "0"
    
    func loadTotals() {
        let service = CovidService()
        service.getTotals { result in
            self.total = result?.confirmed.format() ?? "0"
            self.deaths = result?.deaths.format() ?? "0"
            self.recovered = result?.recovered.format() ?? "0"
            self.recovering = result?.active.format() ?? "0"
        }
    }
}
