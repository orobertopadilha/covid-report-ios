//
//  CountryDataViewModel.swift
//  CovidReport
//
//  Created by Roberto Padilha on 26/09/20.
//  Copyright © 2020 Roberto Padilha. All rights reserved.
//

import Foundation

final class CountryDataViewModel : ObservableObject{
    
    @Published public var total: String = "0"
    @Published public var deaths: String = "0"
    @Published public var recovered: String = "0"
    @Published public var recovering: String = "0"
    @Published public var newCases: String = "0"
    @Published public var newDeaths: String = "0"
    
    func loadTotals(country: String) {
        let service = CovidService()
        service.getDataByCountry(country: country) { result in
            self.total = result?.total.toStrNumber() ?? "0"
            self.deaths = result?.deaths.toStrNumber() ?? "0"
            self.recovered = result?.recovered.toStrNumber() ?? "0"
            self.recovering = result?.recovering.toStrNumber() ?? "0"
            self.newCases = result?.newCases.toStrNumber() ?? "0"
            self.newDeaths = result?.newDeaths.toStrNumber() ?? "0"
        }
    }
}
