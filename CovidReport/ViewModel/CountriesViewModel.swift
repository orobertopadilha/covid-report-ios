//
//  CountriesViewModel.swift
//  CovidReport
//
//  Created by Roberto Padilha on 26/09/20.
//  Copyright © 2020 Roberto Padilha. All rights reserved.
//

import Foundation

final class CountriesViewModel : ObservableObject{
    
    @Published var countries: [Country] = []
    
    init() {
        
        for code in NSLocale.isoCountryCodes as [String] {
            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            let name = NSLocale(localeIdentifier: "en_US").displayName(forKey: NSLocale.Key.identifier, value: id) ??
                "No country for code: \(code)"
            self.countries.append(Country(id: code, name: name))
        }
        
        self.countries.sort(by: { one, two in
            return one.name < two.name
        })
    }
    
}
