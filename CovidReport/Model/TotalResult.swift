//
//  TotalResponse.swift
//  CovidReport
//
//  Created by Roberto Padilha on 25/09/20.
//  Copyright © 2020 Roberto Padilha. All rights reserved.
//

import Foundation

struct TotalResult: Codable {
    
    public var date: String
    public var last_update: String
    public var confirmed: Int
    public var confirmed_diff: Int
    public var recovered: Int
    public var recovered_diff: Int
    public var active : Int
    public var active_diff : Int
    public var deaths: Int
    public var deaths_diff: Int
    public var fatality_rate: Float
}
