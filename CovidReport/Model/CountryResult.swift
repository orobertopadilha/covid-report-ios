//
//  CountryResult.swift
//  CovidReport
//
//  Created by Roberto Padilha on 26/09/20.
//  Copyright © 2020 Roberto Padilha. All rights reserved.
//

import Foundation

struct CountryResult : Codable{
    public var recovering: String
    public var country: String
    public var lastUpdate: String
    public var newCases: String
    public var newDeaths: String
    public var total: String
    public var deaths: String
    public var recovered: String
    
    enum CodingKeys: String, CodingKey {
        case recovering = "Active Cases_text"
        case country = "Country_text"
        case lastUpdate = "Last Update"
        case newCases = "New Cases_text"
        case newDeaths = "New Deaths_text"
        case total = "Total Cases_text"
        case deaths = "Total Deaths_text"
        case recovered = "Total Recovered_text"
    }
}
