//
//  HomeCardView.swift
//  CovidReport
//
//  Created by Roberto Padilha on 24/09/20.
//  Copyright © 2020 Roberto Padilha. All rights reserved.
//

import SwiftUI

struct CardView: View {
    
    @Binding var label: String
    @Binding var value: String
    
    var body: some View {
        VStack(spacing: 4) {
            
            HStack {
                Text(label.uppercased())
                    .font(.caption)
                    .fontWeight(.semibold)
                    .foregroundColor(Color.gray)
                    .frame(maxWidth: .infinity,
                           alignment: .leading)
            }
            
            HStack {
                Text(value)
                    .scaledToFit()
                    .minimumScaleFactor(0.75)
                    .font(.title)
                    .frame(maxWidth: .infinity,
                           alignment: .leading)
            }
        }
        .padding()
        .background(Rectangle().fill(Color.white))
        .cornerRadius(10)
        .clipped()        
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(label: .constant("total cases"), value: .constant("0"))
    }
}
