//
//  CountryList.swift
//  CovidReport
//
//  Created by Roberto Padilha on 25/09/20.
//  Copyright © 2020 Roberto Padilha. All rights reserved.
//

import SwiftUI

struct CountriesView: View {
    
    @ObservedObject private var viewModel = CountriesViewModel()
    
    var body: some View {
        
        List {
            ForEach(self.viewModel.countries) { country in
                CountryItemView(country: country)
            }
        }
    }
}

struct CountriesView_Previews: PreviewProvider {
    static var previews: some View {
        CountriesView()
    }
}
