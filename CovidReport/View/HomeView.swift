import SwiftUI

struct HomeView: View {
    
    @ObservedObject private var viewModel = HomeViewModel()
    
    var body: some View {
        GeometryReader { geometry in
            ZStack() {
                VStack {
                    Rectangle()
                        .foregroundColor(Color("AppBlue"))
                        .frame(height: geometry.size.height / 2)
                    
                    Spacer()
                }
                
                VStack {
                    
                    Spacer().frame(height: geometry.size.height / 4)
                    
                    Text("Global Statistics")
                        .font(.title)
                        .fontWeight(.light)
                        .foregroundColor(Color.white)
                        .frame(maxWidth: .infinity,
                               alignment: .trailing)
                        .padding(.bottom, 8)
                    
                    CardView(label: .constant("Total Cases"), value: self.$viewModel.total).shadow(radius: 4)
                    
                    Spacer().frame(height: 16)
                    CardView(label: .constant("Total Deaths"), value: self.$viewModel.deaths).shadow(radius: 4)
                    
                    Spacer().frame(height: 16)
                    HStack {
                        CardView(label: .constant("Recovered"), value: self.$viewModel.recovered).shadow(radius: 4)
                        CardView(label: .constant("Recovering"), value: self.$viewModel.recovering).shadow(radius: 4)
                    }
                    
                    Spacer()
                }
                
                .padding(.horizontal, 16)
                
            }.edgesIgnoringSafeArea(.all)
        }
        .onAppear(perform: {
            self.viewModel.loadTotals()
        })
        
    }
}

#if DEBUG
struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
#endif
