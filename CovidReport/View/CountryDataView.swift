//
//  CountryDataView.swift
//  CovidReport
//
//  Created by Roberto Padilha on 26/09/20.
//  Copyright © 2020 Roberto Padilha. All rights reserved.
//

import SwiftUI

struct CountryDataView: View {
    
    @ObservedObject private var viewModel = CountryDataViewModel()
    @State var country: Country
    
    var body: some View {
       /* ZStack {
            
            Color("AppBlue").edgesIgnoringSafeArea(.all)*/
            
            VStack {
                Text("Statistics for \(self.country.name)")
                    .font(.largeTitle)
                    .foregroundColor(Color("AppBlue"))
                    .multilineTextAlignment(.center)
                
                Spacer().frame(height: 48)
                
                CardView(label: .constant("Total Cases"), value: self.$viewModel.total).shadow(radius: 4)
                
                Spacer().frame(height: 24)
                CardView(label: .constant("Total Deaths"), value: self.$viewModel.deaths).shadow(radius: 4)
                
                Spacer().frame(height: 24)
                HStack {
                    CardView(label: .constant("New Cases"), value: self.$viewModel.newCases).shadow(radius: 4)
                    CardView(label: .constant("New Deaths"), value: self.$viewModel.newDeaths).shadow(radius: 4)
                }
                
                Spacer().frame(height: 24)
                HStack {
                    CardView(label: .constant("Total Recovered"), value: self.$viewModel.recovered).shadow(radius: 4)
                    CardView(label: .constant("Total Recovering"), value: self.$viewModel.recovering).shadow(radius: 4)
                }
                
            }
            .padding()
            .onAppear(perform: {
                self.viewModel.loadTotals(country: self.country.name)
            })
        //}
    }
}

struct CountryDataView_Previews: PreviewProvider {
    static var previews: some View {
        CountryDataView(country: Country(id: "BR", name: "Brazil"))
    }
}
