//
//  CountryItemView.swift
//  CovidReport
//
//  Created by Roberto Padilha on 26/09/20.
//  Copyright © 2020 Roberto Padilha. All rights reserved.
//

import SwiftUI

struct CountryItemView: View {

    public var country: Country

    @State var open = false
    
    var body: some View {
        Button(action: {
            self.open.toggle()
        }){
            Text(country.name)
        }.sheet(isPresented: self.$open) {
            CountryDataView(country: self.country)
        }
    }
}

struct CountryItemView_Previews: PreviewProvider {
    static var previews: some View {
        CountryItemView(country: Country(id: "BRA", name: "Brazil"))
    }
}
