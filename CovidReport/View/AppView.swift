//
//  AppView.swift
//  CovidReport
//
//  Created by Roberto Padilha on 16/09/20.
//  Copyright © 2020 Roberto Padilha. All rights reserved.
//

import SwiftUI

struct AppView: View {
    
    var body: some View {
        TabView(selection: .constant(0)) {
            NavigationView {
                HomeView()
                    .navigationBarTitle("Covid Report")
            }
            .tabItem{
                Image(systemName: "house.fill")
                Text("Home").font(.headline)
            }
            .tag(0)
            .edgesIgnoringSafeArea(.top)
            
            NavigationView {
                CountriesView()
                .navigationBarTitle("Choose a country")
            }
            .tabItem{
                Image(systemName: "map.fill")
                Text("By Country").font(.headline)
            }
            .tag(1)
            .edgesIgnoringSafeArea(.top)
        }
        .edgesIgnoringSafeArea(.all)
    }
}

struct AppView_Previews: PreviewProvider {
    static var previews: some View {
        AppView()
    }
}
