//
//  NumberUtils.swift
//  CovidReport
//
//  Created by Roberto Padilha on 26/09/20.
//  Copyright © 2020 Roberto Padilha. All rights reserved.
//

import Foundation

extension Int {

    func format() -> String{
        let fmt = NumberFormatter()
        fmt.groupingSeparator = "."
        fmt.usesGroupingSeparator = true
        fmt.groupingSize = 3
        
        return fmt.string(from: NSNumber(value: self)) ?? "0"
    }
}
