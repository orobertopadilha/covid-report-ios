//
//  StringExtensions.swift
//  CovidReport
//
//  Created by Roberto Padilha on 26/09/20.
//  Copyright © 2020 Roberto Padilha. All rights reserved.
//

import Foundation

extension String {

    func toStrNumber() -> String{
        return self.replacingOccurrences(of: ",", with: ".")
    }
}
